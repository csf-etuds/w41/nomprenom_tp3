<!--
titre: Nom de la fonctionnalité (lié au billet)
-->

## Description de la fonctionnalité

<!-- Numéro de l'issue et/ou information de la fonctionnalité développée -->

## Sous-tâches de la tâche

- [ ] Toutes les tâches ont été complétées

<!-- Si ce n'est pas le cas, de nouvelles issues ont été créées? -->

## Prêt pour la revue de code

- [ ] Développement terminé
  - [ ] Code
  - [ ] Tests unitaires

## Tests et couverture de tests

- [ ] La couverture de test est restée la même ou a augmenté

<!-- Si la couverture a diminué, expliquer sommairement la raison -->

## Révision du code

- [ ] Le développeur a fait une révision et a annoté son code
