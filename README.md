# Catalogue

Projet de travail pratique (3) pour le cours de Services Web du Cégep de Sainte-Foy.

## Équipe de réalisation

### Projet de base

* Pascal Lavoie [plavoie@csfoy.ca](mailto:plavoie@csfoy.ca)
* Jean-François Dion [jfdion@csfoy.ca](mailto:plavoie@csfoy.ca)

### Travail pratique

* Prénom nom (membre A) `@TODO: Compléter avec vos informations`
* Prénom nom (membre B) `@TODO: Compléter avec vos informations`

| Récits                             | Membre de l'équipe                                                    |
|------------------------------------|-----------------------------------------------------------------------|
| Fournisseur / Supplier (A, B, C)   | `@TODO compléter avec le nom de la personne ayant réalisé ces récits` |
| Produit / Product (D, E, F)        | `@TODO compléter avec le nom de la personne ayant réalisé ces récits` |
| ---------------------------------- | --------------------                                                  |

## Synopsis de l'application

> @TODO compléter la présentation du projet (en français) 
> À compléter (Requis technique - Documentation README.md)  
> Pourquoi? Qu'est-ce quelle permet? Pour qui? etc.

## Installation

Forker le Projet dans votre groupe 'servicesweb'
Dans votre IDE préféré, importer un projet maven avec ces fichiers
Lancer l'application par le main.

## Usage

### Démarrage de l'application

> @TODO compléter la documentation pour démarrer le projet (en français)
> À compléter (Requis technique - Documentation README.md)  
> À patir de l'IDE (Eclipse OU IntelliJ - Un des 2)
> Avec Docker

Utiliser Postman pour vérifier vos routes et obtenir et modifier les ressources du service web.

Une collection [Postman](./TP3-Catalogue.postman_collection.json) est disponible pour le projet

### Mise à jour de la collection postman à partir d'OpenAPI

Allez sur la page de [documentation OpenAPI](http://localhost:8080/v3/api-docs) de votre application

Le schéma JSON de la documentation d'OpenAPI peut être copié.

Dans Postman sélectionnez importer, puis collez le schéma dans le champ texte.

Une fois les changements faits, vous pouvez exporter la collection et écraser le fichier de collection à la racine de ce
dépôt.

### Base de données en mémoire (H2)

Vérification et exportation des données, allez sur
la [page Web de la console de la base de données](http://localhost:8080/h2-console).

Assurez-vous d'avoir les informations suivantes

**Driver class** `org.h2.Driver`  
**JDBC URL** `jdbc:h2:mem:catalog`  
**User name** `sa`  
**Password** `""` -- laissez le champ vide

## Contribution

Aucune contribution n'est accepté

## License

MIT