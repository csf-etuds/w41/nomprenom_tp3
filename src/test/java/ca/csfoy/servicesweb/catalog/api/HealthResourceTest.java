package ca.csfoy.servicesweb.catalog.api;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@Tag("Api")
@SpringBootTest
@AutoConfigureMockMvc
public class HealthResourceTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void healthCheckReturnStatusOk() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get(HealthResource.RESOURCE_PATH)
                                          .contentType(MediaType.APPLICATION_JSON))
           .andExpect(MockMvcResultMatchers.status().isOk());

    }
}
