package ca.csfoy.servicesweb.catalog.api.dto;

import ca.csfoy.servicesweb.catalog.api.UserValidationConstants;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Set;

@SpringBootTest
class FullUserDtoTest {

    @Autowired
    private Validator validator;

    @Test
    public void fullUserDtoWithValidValuesHasNoConstraintViolation() {
        FullUserDto dto = UserDtoMother.fullUserDto();

        Set<ConstraintViolation<FullUserDto>> result = validator.validate(dto);

        Assertions.assertEquals(0, result.size());
    }

    @Test
    public void fullUserDtoWithInvalidIdIsInvalid() {
        FullUserDto dto = UserDtoMother.fullUserDtoWithDefault(UserDtoMother.ID_INVALID_FORMAT, null, null, null, null);

        Set<ConstraintViolation<FullUserDto>> result = validator.validate(dto);
        List<String> messages = extractMessages(result);

        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_UUID_FORMAT_VALIDATION));
    }

    @Test
    public void fullUserDtoWithBlankFirstnameIsInvalid() {
        FullUserDto dto = UserDtoMother.fullUserDtoWithDefault(null, UserDtoMother.FIRSTNAME_BLANK, null, null, null);

        Set<ConstraintViolation<FullUserDto>> result = validator.validate(dto);
        List<String> messages = extractMessages(result);

        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_STR_NOT_NULL_VALIDATION));
    }

    @Test
    public void fullUserDtoWithFirstnameTooLongIsInvalid() {
        FullUserDto dto =
                UserDtoMother.fullUserDtoWithDefault(null, UserDtoMother.FIRSTNAME_TOO_LONG, null, null, null);

        Set<ConstraintViolation<FullUserDto>> result = validator.validate(dto);
        List<String> messages = extractMessageTemplates(result);

        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_STR_MIN_MAX_LENGTH_VALIDATION));
    }

    @Test
    public void fullUserDtoWithBlankLastnameIsInvalid() {
        FullUserDto dto = UserDtoMother.fullUserDtoWithDefault(null, null, UserDtoMother.LASTNAME_BLANK, null, null);

        Set<ConstraintViolation<FullUserDto>> result = validator.validate(dto);
        List<String> messages = extractMessages(result);

        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_STR_NOT_NULL_VALIDATION));
    }

    @Test
    public void fullUserDtoWithLastnameTooLongIsInvalid() {
        FullUserDto dto = UserDtoMother.fullUserDtoWithDefault(null, null, UserDtoMother.LASTNAME_TOO_LONG, null, null);

        Set<ConstraintViolation<FullUserDto>> result = validator.validate(dto);
        List<String> messages = extractMessageTemplates(result);

        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_STR_MIN_MAX_LENGTH_VALIDATION));
    }

    @Test
    public void fullUserDtoWithBlankEmailAddressIsInvalid() {
        FullUserDto dto = UserDtoMother.fullUserDtoWithDefault(null, null, null, UserDtoMother.EMAIL_BLANK, null);

        Set<ConstraintViolation<FullUserDto>> result = validator.validate(dto);
        List<String> messages = extractMessages(result);

        Assertions.assertEquals(2, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_EMAIL_NOT_NULL_VALIDATION));
    }

    @Test
    public void fullUserDtoWithInvalidEmailAddressIsInvalid() {
        FullUserDto dto = UserDtoMother.fullUserDtoWithDefault(null, null, null, UserDtoMother.EMAIL_INVALID, null);

        Set<ConstraintViolation<FullUserDto>> result = validator.validate(dto);
        List<String> messages = extractMessageTemplates(result);

        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_FORMAT_EMAIL_VALIDATION));
    }

    @Test
    public void fullUserDtoWithBlankPasswordIsInvalid() {
        FullUserDto dto = UserDtoMother.fullUserDtoWithDefault(null, null, null, null, UserDtoMother.PASSWORD_BLANK);

        Set<ConstraintViolation<FullUserDto>> result = validator.validate(dto);
        List<String> messages = extractMessages(result);

        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_PWD_NOT_NULL_VALIDATION));
    }

    @Test
    public void fullUserDtoWithPasswordTooLongIsInvalid() {
        FullUserDto dto = UserDtoMother.fullUserDtoWithDefault(null, null, null, null, UserDtoMother.PASSWORD_TOO_LONG);

        Set<ConstraintViolation<FullUserDto>> result = validator.validate(dto);
        List<String> messages = extractMessageTemplates(result);

        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_PWD_MAX_LENGTH_VALIDATION));
    }

    private List<String> extractMessages(Set<ConstraintViolation<FullUserDto>> result) {
        return result.stream().map(ConstraintViolation::getMessage).toList();
    }

    private List<String> extractMessageTemplates(Set<ConstraintViolation<FullUserDto>> result) {
        return result.stream().map(ConstraintViolation::getMessageTemplate).toList();
    }

}