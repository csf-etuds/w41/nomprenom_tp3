package ca.csfoy.servicesweb.catalog.api.dto;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;

@SpringBootTest
class UserCredentialsDtoTest {

    @Autowired
    private Validator validator;

    @Test
    public void userCredentialDtoWithValidValuesHasNoConstraintViolation() {
        UserCredentialsDto dto = UserDtoMother.userCredentialsDto();

        Set<ConstraintViolation<UserCredentialsDto>> result = validator.validate(dto);

        Assertions.assertEquals(0, result.size());
    }
}