package ca.csfoy.servicesweb.catalog.api.dto;

public class UserDtoMother {

    public static final String ID = "e4008169-d989-4b7d-88fb-11f3bfd8f380";
    public static final String ID_BLANK = "    ";
    public static final String ID_INVALID_FORMAT = "not-a-uuid";

    public static final String FIRSTNAME = "Joe";
    public static final String FIRSTNAME_BLANK = "    ";
    public static final String FIRSTNAME_TOO_LONG = "aaaaaaaaaa_aaaaaaaaaa_aaaaaaaaaa";

    public static final String LASTNAME = "Bean";
    public static final String LASTNAME_BLANK = "    ";
    public static final String LASTNAME_TOO_LONG = "aaaaaaaaaa_aaaaaaaaaa_aaaaaaaaaa";

    public static final String EMAIL = "aa@aa.ca";
    public static final String EMAIL_BLANK = "    ";
    public static final String EMAIL_INVALID = "invalid-email";

    public static final String PASSWORD = "correcthorsebatterystaple";
    public static final String PASSWORD_BLANK = "    ";
    public static final String PASSWORD_TOO_SHORT = "a";
    public static final String PASSWORD_TOO_LONG = "aaaaaaaaaa_aaaaaaaaaa_aaaaaaaaaa_aaaaaaaaaa_aaaaaaaaaa_aaaaaaaaaa";

    public static UserDto userDto() {
        return new UserDto(ID, FIRSTNAME, LASTNAME);
    }

    public static UserDto userDto(String id) {
        return new UserDto(id, FIRSTNAME, LASTNAME);
    }

    public static UserDto userDtoWithDefault(String id, String firstname, String lastname) {
        return new UserDto(
                valueOrDefault(id, ID),
                valueOrDefault(firstname, FIRSTNAME),
                valueOrDefault(lastname, LASTNAME)
        );
    }

    public static FullUserDto fullUserDto() {
        return new FullUserDto(ID, FIRSTNAME, LASTNAME, EMAIL, PASSWORD);
    }

    public static FullUserDto fullUserDto(String id) {
        return new FullUserDto(id, FIRSTNAME, LASTNAME, EMAIL, PASSWORD);
    }

    public static FullUserDto fullUserDtoWithDefault(String id, String firstname, String lastname, String emailAddress,
                                                     String password
    ) {
        return new FullUserDto(
                valueOrDefault(id, ID),
                valueOrDefault(firstname, FIRSTNAME),
                valueOrDefault(lastname, LASTNAME),
                valueOrDefault(emailAddress, EMAIL),
                valueOrDefault(password, PASSWORD)
        );
    }

    public static UserCredentialsDto userCredentialsDto() {
        return new UserCredentialsDto(EMAIL, PASSWORD);
    }

    public static UserCredentialsDto userCredentialsDtoWithDefault(String email, String password) {
        return new UserCredentialsDto(valueOrDefault(email, EMAIL), valueOrDefault(password, PASSWORD));
    }

    private static String valueOrDefault(String value, String defaultValue) {
        return value != null ? value : defaultValue;
    }
}
