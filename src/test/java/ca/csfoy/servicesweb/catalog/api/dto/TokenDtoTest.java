package ca.csfoy.servicesweb.catalog.api.dto;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;

@SpringBootTest
class TokenDtoTest {

    @Autowired
    private Validator validator;

    @Test
    public void tokenDtoWithValidValuesHasNoConstraintViolation() {
        TokenDto dto = new TokenDto("some-token");

        Set<ConstraintViolation<TokenDto>> result = validator.validate(dto);

        Assertions.assertEquals(0, result.size());
    }
}