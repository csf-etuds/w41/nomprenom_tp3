package ca.csfoy.servicesweb.catalog.api.dto;

import ca.csfoy.servicesweb.catalog.api.UserValidationConstants;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Set;

@SpringBootTest
class UserDtoTest {

    @Autowired
    private Validator validator;

    @Test
    public void userDtoWithValidValuesHasNoConstraintViolation() {
        UserDto dto = UserDtoMother.userDto();

        Set<ConstraintViolation<UserDto>> result = validator.validate(dto);

        Assertions.assertEquals(0, result.size());
    }

    @Test
    public void userDtoWithBlankIdIsInvalid() {
        UserDto dto = UserDtoMother.userDtoWithDefault(UserDtoMother.ID_BLANK, null, null);

        Set<ConstraintViolation<UserDto>> result = validator.validate(dto);
        List<String> messages = extractMessages(result);

        Assertions.assertEquals(2, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_UUID_NOT_NULL_VALIDATION));
    }

    @Test
    public void userDtoWithInvalidIdIsInvalid() {
        UserDto dto = UserDtoMother.userDtoWithDefault(UserDtoMother.ID_INVALID_FORMAT, null, null);

        Set<ConstraintViolation<UserDto>> result = validator.validate(dto);
        List<String> messages = extractMessages(result);

        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_UUID_FORMAT_VALIDATION));
    }

    @Test
    public void userDtoWithBlankFirstnameIsInvalid() {
        UserDto dto = UserDtoMother.userDtoWithDefault(null, UserDtoMother.FIRSTNAME_BLANK, null);

        Set<ConstraintViolation<UserDto>> result = validator.validate(dto);
        List<String> messages = extractMessages(result);

        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_STR_NOT_NULL_VALIDATION));
    }

    @Test
    public void userDtoWithFirstnameTooLongIsInvalid() {
        UserDto dto = UserDtoMother.userDtoWithDefault(null, UserDtoMother.FIRSTNAME_TOO_LONG, null);

        Set<ConstraintViolation<UserDto>> result = validator.validate(dto);
        List<String> messages = extractMessageTemplates(result);

        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_STR_MIN_MAX_LENGTH_VALIDATION));
    }

    @Test
    public void userDtoWithBlankLastnameIsInvalid() {
        UserDto dto = UserDtoMother.userDtoWithDefault(null, null, UserDtoMother.LASTNAME_BLANK);

        Set<ConstraintViolation<UserDto>> result = validator.validate(dto);
        List<String> messages = extractMessages(result);

        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_STR_NOT_NULL_VALIDATION));
    }

    @Test
    public void userDtoWithLastnameTooLongIsInvalid() {
        UserDto dto = UserDtoMother.userDtoWithDefault(null, null, UserDtoMother.LASTNAME_TOO_LONG);

        Set<ConstraintViolation<UserDto>> result = validator.validate(dto);
        List<String> messages = extractMessageTemplates(result);

        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(messages.contains(UserValidationConstants.MSG_STR_MIN_MAX_LENGTH_VALIDATION));
    }

    private List<String> extractMessages(Set<ConstraintViolation<UserDto>> result) {
        return result.stream().map(ConstraintViolation::getMessage).toList();
    }

    private List<String> extractMessageTemplates(Set<ConstraintViolation<UserDto>> result) {
        return result.stream().map(ConstraintViolation::getMessageTemplate).toList();
    }
}