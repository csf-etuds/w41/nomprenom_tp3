package ca.csfoy.servicesweb.catalog.api.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class PaginatedResultsDtoTest {

    public static final int FIRST_PAGE = 1;
    public static final int TOTAL_PAGES = 1;
    public static final int TOTAL_ENTRIES = 0;

    @Test
    public void newPaginatedResultsIsValid() {
        List<Object> emptyList = new ArrayList<>();

        PaginatedResultsDto<Object> result = new PaginatedResultsDto<>(
                emptyList,
                FIRST_PAGE,
                TOTAL_PAGES,
                TOTAL_ENTRIES
        );

        Assertions.assertEquals(emptyList, result.getContent());
        Assertions.assertEquals(FIRST_PAGE, result.getCurrentPage());
        Assertions.assertEquals(TOTAL_PAGES, result.getTotalPages());
        Assertions.assertEquals(TOTAL_ENTRIES, result.getTotalEntries());
    }

}