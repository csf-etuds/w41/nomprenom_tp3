package ca.csfoy.servicesweb.catalog.api;

import ca.csfoy.servicesweb.catalog.api.dto.TokenDto;
import ca.csfoy.servicesweb.catalog.api.dto.UserCredentialsDto;
import ca.csfoy.servicesweb.catalog.security.JwtTokenManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@Tag("Api")
@SpringBootTest
@AutoConfigureMockMvc
public class LoginResourceTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    JwtTokenManager tokenManager;

    @Test
    public void validLoginWillProduceAToken() throws Exception {
        UserCredentialsDto dto = new UserCredentialsDto(
                "admin@web.ca",
                "Bonjour123"
        );

        ResultActions result = mvc.perform(MockMvcRequestBuilders.post(LoginResource.RESOURCE_PATH)
                                                                 .contentType(MediaType.APPLICATION_JSON)
                                                                 .content(mapper.writeValueAsString(dto)));

        String response = result.andReturn().getResponse().getContentAsString();
        TokenDto token = mapper.readValue(response, TokenDto.class);

        result.andExpect(MockMvcResultMatchers.status().isOk());
        Assertions.assertTrue(tokenManager.validateToken(token.token));
    }

    @Test
    public void loginInvalidIsUnauthorized() throws Exception {
        UserCredentialsDto dto = new UserCredentialsDto(
                "admin@web.ca",
                "Bonjour456"
        );

        ResultActions result = mvc.perform(MockMvcRequestBuilders.post(LoginResource.RESOURCE_PATH)
                                                                 .contentType(MediaType.APPLICATION_JSON)
                                                                 .content(mapper.writeValueAsString(dto)))
                                  .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }
}
