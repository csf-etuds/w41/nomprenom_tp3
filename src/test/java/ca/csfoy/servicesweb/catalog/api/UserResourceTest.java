package ca.csfoy.servicesweb.catalog.api;

import ca.csfoy.servicesweb.catalog.api.dto.FullUserDto;
import ca.csfoy.servicesweb.catalog.api.dto.UserDto;
import ca.csfoy.servicesweb.catalog.api.dto.UserDtoMother;
import ca.csfoy.servicesweb.catalog.controller.exception.ObjectNotFoundException;
import ca.csfoy.servicesweb.catalog.domain.user.CatalogUser;
import ca.csfoy.servicesweb.catalog.domain.user.UserRepository;
import ca.csfoy.servicesweb.catalog.infra.user.UserDao;
import ca.csfoy.servicesweb.catalog.infra.user.UserEntity;
import ca.csfoy.servicesweb.catalog.infra.user.UserEntityConverter;
import ca.csfoy.servicesweb.catalog.infra.user.UserEntityMother;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@Tag("Api")
@SpringBootTest
@AutoConfigureMockMvc
@WithUserDetails("user@web.ca")
public class UserResourceTest {

    public static final String ID = UserDtoMother.ID;
    public static final String EMAIL = UserDtoMother.EMAIL;
    public static final String NOT_A_UUID = "1";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserEntityConverter userEntityConverter;

    @AfterEach
    public void tearDown() {
        try {
            CatalogUser user = userRepository.getBy(ID);
            if (user != null) {
                userDao.delete(userEntityConverter.fromUser(user));
            }

            user = userRepository.getByEmail(EMAIL);
            if (user != null) {
                userDao.delete(userEntityConverter.fromUser(user));
            }

        } catch (Exception e) {
            // nothing
        }
    }

    @Test
    @WithAnonymousUser
    public void creatingAUserFromFullUserDtoWillCreateANewCatalogUser() throws Exception {
        FullUserDto dto = UserDtoMother.fullUserDto(ID);

        ResultActions result =
                mvc.perform(MockMvcRequestBuilders.post(UserResource.RESOURCE_PATH.concat(UserResource.PATH_SIGN_UP))
                                                  .contentType(MediaType.APPLICATION_JSON)
                                                  .content(mapper.writeValueAsString(dto)));

        result.andExpect(MockMvcResultMatchers.status().isCreated());
        CatalogUser user = userRepository.getBy(dto.id);

        Assertions.assertEquals(dto.firstname, user.getFirstname());
        Assertions.assertEquals(dto.lastname, user.getLastname());
        Assertions.assertEquals(dto.emailAddress, user.getEmailAddress());
        Assertions.assertNotNull(user.getPassword());
    }

    @Test
    @WithAnonymousUser
    public void creatingAUserFromFullUserDtoWithoutIdWillCreateANewCatalogUser() throws Exception {
        FullUserDto dto = UserDtoMother.fullUserDto(null);

        ResultActions result =
                mvc.perform(MockMvcRequestBuilders.post(UserResource.RESOURCE_PATH.concat(UserResource.PATH_SIGN_UP))
                                                  .contentType(MediaType.APPLICATION_JSON)
                                                  .content(mapper.writeValueAsString(dto)));

        result.andExpect(MockMvcResultMatchers.status().isCreated());
        CatalogUser user = userRepository.getByEmail(dto.emailAddress);

        Assertions.assertEquals(dto.firstname, user.getFirstname());
        Assertions.assertEquals(dto.lastname, user.getLastname());
        Assertions.assertEquals(dto.emailAddress, user.getEmailAddress());
        Assertions.assertNotNull(user.getPassword());
    }

    @Test
    @WithAnonymousUser
    public void creatingAUserFromFullUserDtoWithErrorsWillNotCreateANewCatalogUser() throws Exception {
        FullUserDto dto = UserDtoMother.fullUserDtoWithDefault(null, "", "", null, null);

        ResultActions result =
                mvc.perform(MockMvcRequestBuilders.post(UserResource.RESOURCE_PATH.concat(UserResource.PATH_SIGN_UP))
                                                  .contentType(MediaType.APPLICATION_JSON)
                                                  .content(mapper.writeValueAsString(dto)));

        // @TODO Ajuster pour le bon code de retour HTTP
        result.andExpect(MockMvcResultMatchers.status().isInternalServerError());
        Assertions.assertThrows(ObjectNotFoundException.class, () -> userRepository.getByEmail(null));
    }

    @Test
    public void gettingUserThatDoesNotExistWillNotReturnUser() throws Exception {
        ResultActions result = mvc.perform(
                MockMvcRequestBuilders.get(UserResource.RESOURCE_PATH.concat(UserResource.PATH_WITH_ID), ID)
                                      .contentType(MediaType.APPLICATION_JSON));

        // @TODO Ajuster pour le bon code de retour HTTP
        result.andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    @Test
    public void gettingUserWithInvalidIdWillReturnBadRequest() throws Exception {
        ResultActions result = mvc.perform(
                MockMvcRequestBuilders.get(UserResource.RESOURCE_PATH.concat(UserResource.PATH_WITH_ID), NOT_A_UUID)
                                      .contentType(MediaType.APPLICATION_JSON));

        // @TODO Ajuster pour le bon code de retour HTTP
        result.andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    @Test
    public void gettingUserThatDoesExistWillReturnUser() throws Exception {
        UserEntity userEntity = UserEntityMother.userEntity(ID);
        userDao.save(userEntity);
        UserDto userDto = UserDtoMother.userDto(ID);

        ResultActions result = mvc.perform(
                MockMvcRequestBuilders.get(UserResource.RESOURCE_PATH.concat(UserResource.PATH_WITH_ID), ID)
                                      .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk());

        String response = result.andReturn().getResponse().getContentAsString();
        UserDto responseDto = mapper.readValue(response, UserDto.class);

        Assertions.assertEquals(userDto.id, responseDto.id);
        Assertions.assertEquals(userDto.firstname, responseDto.firstname);
        Assertions.assertEquals(userDto.lastname, responseDto.lastname);
    }

    @Test
    public void modifyingAUserThatDoesNotExistWillNotModifyUser() throws Exception {
        UserDto userDto = UserDtoMother.userDto(ID);

        ResultActions result = mvc.perform(
                MockMvcRequestBuilders.put(UserResource.RESOURCE_PATH.concat(UserResource.PATH_WITH_ID), ID)
                                      .contentType(MediaType.APPLICATION_JSON)
                                      .content(mapper.writeValueAsString(userDto)));

        // @TODO Ajuster pour le bon code de retour HTTP
        result.andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    @Test
    public void modifyingAUserThatDoesExistWillModifyUser() throws Exception {
        UserEntity userEntity = UserEntityMother.userEntity(ID);
        userDao.save(userEntity);
        UserDto userDto = UserDtoMother.userDtoWithDefault(ID, "new-firstname", "new-lastname");

        ResultActions result = mvc.perform(
                MockMvcRequestBuilders.put(UserResource.RESOURCE_PATH.concat(UserResource.PATH_WITH_ID), ID)
                                      .contentType(MediaType.APPLICATION_JSON)
                                      .content(mapper.writeValueAsString(userDto)));

        result.andExpect(MockMvcResultMatchers.status().isNoContent());

        CatalogUser modifiedUser = userRepository.getBy(ID);
        Assertions.assertEquals(userDto.firstname, modifiedUser.getFirstname());
        Assertions.assertEquals(userDto.lastname, modifiedUser.getLastname());
    }

    @Test
    public void modifyingAUserWithInvalidIdWillReturnBadRequest() throws Exception {
        UserDto userDto = UserDtoMother.userDto(ID);

        ResultActions result = mvc.perform(
                MockMvcRequestBuilders.put(UserResource.RESOURCE_PATH.concat(UserResource.PATH_WITH_ID), NOT_A_UUID)
                                      .contentType(MediaType.APPLICATION_JSON)
                                      .content(mapper.writeValueAsString(userDto)));

        // @TODO Ajuster pour le bon code de retour HTTP
        result.andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }
}
