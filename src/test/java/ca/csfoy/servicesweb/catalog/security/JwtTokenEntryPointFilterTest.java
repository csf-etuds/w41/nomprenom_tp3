package ca.csfoy.servicesweb.catalog.security;

import ca.csfoy.servicesweb.catalog.controller.CatalogProblemDetailFactory;
import ca.csfoy.servicesweb.catalog.domain.user.CatalogUserDetails;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ProblemDetail;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.io.IOException;

@ExtendWith(MockitoExtension.class)
class JwtTokenEntryPointFilterTest {

    public static final String EMAIL = "jean.tremblay@testcsfoy.ca";
    public static final String ID = "1";
    public static final String PASSWORD = "password";

    @Mock
    JwtTokenManager tokenManager;

    @Mock
    UserDetailsService userDetailsService;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpServletResponse response;

    @Mock
    FilterChain filterChain;

    ObjectMapper mapper;

    UserDetails userDetails;

    JwtTokenEntryPointFilter tokenEntryPointFilter;

    @BeforeEach
    public void setUp() {
        tokenEntryPointFilter = new JwtTokenEntryPointFilter(
                new CatalogProblemDetailFactory(),
                tokenManager,
                userDetailsService,
                new ObjectMapper()
        );
        mapper = new ObjectMapper();

        userDetails = new CatalogUserDetails(EMAIL, PASSWORD);

        Mockito.when(request.getHeader("Authorization")).thenReturn(JWTMother.getHeaderAuthorization());
        Mockito.when(tokenManager.validateToken(JWTMother.getToken())).thenReturn(true);
        Mockito.when(tokenManager.getSubjectFromToken(JWTMother.getToken())).thenReturn(EMAIL);
        Mockito.when(tokenManager.getUserIdFromToken(JWTMother.getToken())).thenReturn(ID);
        Mockito.when(userDetailsService.loadUserByUsername(EMAIL)).thenReturn(userDetails);
    }

    @Test
    public void withAValidTokenWillCallNextFilterInChain() throws Exception {
        tokenEntryPointFilter.doFilter(request, response, filterChain);

        Mockito.verify(filterChain, Mockito.times(1)).doFilter(request, response);
    }

    @Test
    public void userNotFoundErrorIsProperlyManaged() throws Exception {
        ServletOutputStream outputStream = Mockito.mock(ServletOutputStream.class);

        Mockito.when(userDetailsService.loadUserByUsername(EMAIL)).thenThrow(UsernameNotFoundException.class);
        Mockito.when(response.getOutputStream()).thenReturn(outputStream);

        tokenEntryPointFilter.doFilter(request, response, filterChain);

        Mockito.verify(response, Mockito.times(1)).setStatus(HttpServletResponse.SC_FORBIDDEN);
        Mockito.verify(response, Mockito.times(1)).setContentType(MediaType.APPLICATION_JSON_VALUE);
        Mockito.verify(response, Mockito.times(1)).setCharacterEncoding("UTF-8");
        Mockito.verify(response, Mockito.times(1)).getOutputStream();
    }

    @Test
    public void userNotFoundErrorReturnDescriptiveMessage() throws Exception {
        ServletOutputStream outputStream = Mockito.mock(ServletOutputStream.class);

        Mockito.when(userDetailsService.loadUserByUsername(EMAIL)).thenThrow(UsernameNotFoundException.class);
        Mockito.when(response.getOutputStream()).thenReturn(outputStream);

        tokenEntryPointFilter.doFilter(request, response, filterChain);

        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        Mockito.verify(outputStream, Mockito.times(1)).println(captor.capture());

        ProblemDetail problemDetail = mapper.readValue(captor.getValue(), ProblemDetail.class);

        Assertions.assertEquals(HttpStatus.FORBIDDEN.value(), problemDetail.getStatus());
        Assertions.assertEquals("Forbidden", problemDetail.getTitle());
        Assertions.assertEquals(true, problemDetail.getProperties().get("expected"));
    }

    @Test
    public void willThrowAnUncheckedExceptionIfErrorOccursWhenHandlingError() throws Exception {
        Mockito.when(userDetailsService.loadUserByUsername(EMAIL)).thenThrow(UsernameNotFoundException.class);
        Mockito.doThrow(IOException.class).when(response).getOutputStream();

        Assertions.assertThrows(RuntimeException.class,
                () -> tokenEntryPointFilter.doFilter(request, response, filterChain));

    }
}