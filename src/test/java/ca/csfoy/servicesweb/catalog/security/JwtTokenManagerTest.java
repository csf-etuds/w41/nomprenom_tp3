package ca.csfoy.servicesweb.catalog.security;

import ca.csfoy.servicesweb.catalog.domain.user.Role;
import ca.csfoy.servicesweb.catalog.domain.user.RoleName;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

@ExtendWith(MockitoExtension.class)
class JwtTokenManagerTest {

    public static final String JWT_VALIDITY_IN_MS = "jwt.validityTimeInMilliseconds";
    public static final String ONE_HOUR_IN_MS = "3155695200000";
    public static final String JWT_SECRET_KEY = "jwt.secretKey";
    public static final String SECRET = "3402c39364237b6a31fa364868dfae1cfe36b4e2f2fce40edd9bdcc13d1f1719";
    public static final String EMAIL = "admin@web.ca";

    @Mock
    Environment env;

    Role role;

    JwtTokenManager tokenManager;

    @BeforeEach
    public void setUp() {
        Mockito.when(env.getProperty(JWT_SECRET_KEY)).thenReturn(SECRET);

        role = new Role("1", RoleName.USER);

        tokenManager = new JwtTokenManager(env);
    }

    @Test
    public void canCreateATokenFromEmailAndRole() {
        Mockito.when(env.getProperty(JWT_VALIDITY_IN_MS)).thenReturn(ONE_HOUR_IN_MS);

        Role role = new Role("1", RoleName.USER);

        String token = tokenManager.createToken(EMAIL, role);

        Assertions.assertNotNull(token);
    }

    @Test
    public void canValidateAToken() {
        boolean result = tokenManager.validateToken(JWTMother.getToken());

        Assertions.assertTrue(result);
    }

    @Test
    public void canExtractUserIDFromToken() {
        String result = tokenManager.getUserIdFromToken(JWTMother.getToken());

        Assertions.assertNull(result);
    }

    @Test
    public void canExtractSubjectFromToken() {
        String result = tokenManager.getSubjectFromToken(JWTMother.getToken());

        Assertions.assertEquals(EMAIL, result);
    }
}