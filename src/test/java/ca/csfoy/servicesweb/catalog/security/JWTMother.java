package ca.csfoy.servicesweb.catalog.security;

public class JWTMother {

    public static final String JWT_TOKEN =
            "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJhZG1pbkB3ZWIuY2EiLCJhdXRoIjp7ImlkZW50aWZpZXIiOiIxIiwicm9sZU5hbWUiOiJVU0VSIiwiYXV0aG9yaXR5IjoiUk9MRV9VU0VSIiwicm9sZVVzZXIiOnRydWUsInJvbGVBZG1pbiI6ZmFsc2V9LCJpYXQiOjE3MTI5Mjc2NzQsImV4cCI6NDg2ODYyMjg3NH0.RsUgi0yl61QJGbJpIAGz6qFXQIeIS0Re4IN8uAFP1Zb1KZf_ZDE4YIBAoOeA4SsA";

    public static final String HEADER_AUTHORIZATION = "Bearer " + JWT_TOKEN;

    public static String getToken() {
        return JWT_TOKEN;
    }

    public static String getHeaderAuthorization() {
        return HEADER_AUTHORIZATION;
    }
}
