package ca.csfoy.servicesweb.catalog.controller;

import ca.csfoy.servicesweb.catalog.api.dto.TokenDto;
import ca.csfoy.servicesweb.catalog.api.dto.UserCredentialsDto;
import ca.csfoy.servicesweb.catalog.api.dto.UserDtoMother;
import ca.csfoy.servicesweb.catalog.domain.user.CatalogUser;
import ca.csfoy.servicesweb.catalog.domain.user.UserMother;
import ca.csfoy.servicesweb.catalog.domain.user.UserRepository;
import ca.csfoy.servicesweb.catalog.security.JwtTokenManager;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

@ExtendWith(MockitoExtension.class)
class LoginControllerTest {

    public static final String TOKEN = "some-expected-token";

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private UserRepository userRepository;

    @Mock
    private JwtTokenManager tokenManager;

    @InjectMocks
    private LoginController loginController;

    private CatalogUser user;
    private UserCredentialsDto userCredentialsDto;

    @BeforeEach
    public void setUp() {
        user = UserMother.getUser();
        userCredentialsDto = UserDtoMother.userCredentialsDtoWithDefault(user.getEmailAddress(), null);
    }

    @Test
    public void loginUserWithValidCredentialsWillReturnToken() {
        Mockito.when(userRepository.getByEmail(user.getEmailAddress())).thenReturn(user);
        Mockito.when(tokenManager.createToken(user.getEmailAddress(), user.getRole())).thenReturn(TOKEN);

        TokenDto result = loginController.loginUser(userCredentialsDto);

        Assertions.assertEquals(TOKEN, result.token);
    }

    @Test
    public void authenticationExceptionWillNotBeCatched() {
        Mockito.doThrow(BadCredentialsException.class).when(authenticationManager).authenticate(Mockito.any(
                UsernamePasswordAuthenticationToken.class));

        Assertions.assertThrows(BadCredentialsException.class, () -> loginController.loginUser(userCredentialsDto));
    }

    @Test
    public void objectNotFoundExceptionWillNotBeCatched() {
        Mockito.doThrow(ObjectNotFoundException.class).when(userRepository).getByEmail(user.getEmailAddress());

        Assertions.assertThrows(ObjectNotFoundException.class, () -> loginController.loginUser(userCredentialsDto));
    }
}