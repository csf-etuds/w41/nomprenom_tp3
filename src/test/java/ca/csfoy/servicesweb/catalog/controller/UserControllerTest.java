package ca.csfoy.servicesweb.catalog.controller;

import ca.csfoy.servicesweb.catalog.api.dto.FullUserDto;
import ca.csfoy.servicesweb.catalog.api.dto.UserDto;
import ca.csfoy.servicesweb.catalog.api.dto.UserDtoMother;
import ca.csfoy.servicesweb.catalog.domain.user.CatalogUser;
import ca.csfoy.servicesweb.catalog.domain.user.UserMother;
import ca.csfoy.servicesweb.catalog.domain.user.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    public static final String ID = UserDtoMother.ID;
    public static final String ENCODED_PASSWORD = "encoded-password";

    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordEncoder passwordEncoder;

    private FullUserDto fullUserDto;

    private CatalogUser catalogUser;

    private UserController userController;

    @BeforeEach
    public void setUp() {
        fullUserDto = UserDtoMother.fullUserDto(ID);
        catalogUser = UserMother.getUser(ID);
        userController = new UserController(userRepository, new UserConverter(passwordEncoder));

    }

    @Test
    public void creatingAUsersFromFullUserDtoStoresAUser() {
        Mockito.when(passwordEncoder.encode(Mockito.anyString())).thenReturn(ENCODED_PASSWORD);

        userController.createUser(fullUserDto);

        ArgumentCaptor<CatalogUser> captor = ArgumentCaptor.forClass(CatalogUser.class);
        Mockito.verify(userRepository, Mockito.times(1)).create(captor.capture());

        CatalogUser user = captor.getValue();
        Assertions.assertEquals(fullUserDto.firstname, user.getFirstname());
        Assertions.assertEquals(fullUserDto.lastname, user.getLastname());
        Assertions.assertEquals(fullUserDto.emailAddress, user.getEmailAddress());
        Assertions.assertEquals(ENCODED_PASSWORD, user.getPassword());

    }

    @Test
    public void creatingAUsersFromFullUserDtoWithoutIdAssignsAnIdStoresAUser() {
        Mockito.when(passwordEncoder.encode(Mockito.anyString())).thenReturn(ENCODED_PASSWORD);

        userController.createUser(UserDtoMother.fullUserDto(null));

        ArgumentCaptor<CatalogUser> captor = ArgumentCaptor.forClass(CatalogUser.class);
        Mockito.verify(userRepository, Mockito.times(1)).create(captor.capture());

        CatalogUser user = captor.getValue();
        Assertions.assertNotNull(user.getId());
    }

    @Test
    public void gettingCatalogUserReturnsUserDto() {
        Mockito.when(userRepository.getBy(ID)).thenReturn(catalogUser);

        UserDto result = userController.getUser(ID);

        Assertions.assertEquals(catalogUser.getFirstname(), result.firstname);
        Assertions.assertEquals(catalogUser.getLastname(), result.lastname);
        Assertions.assertEquals(catalogUser.getId(), result.id);
    }

    @Test
    public void modifyingCatalogUserStoresModifiedUser() {
        Mockito.when(userRepository.getBy(ID)).thenReturn(catalogUser);
        UserDto userDto = UserDtoMother.userDtoWithDefault(ID, "new-firstname", "new-lastname");

        userController.modifyUser(ID, userDto);

        ArgumentCaptor<CatalogUser> captor = ArgumentCaptor.forClass(CatalogUser.class);
        Mockito.verify(userRepository, Mockito.times(1)).save(Mockito.eq(ID), captor.capture());

        Assertions.assertEquals(userDto.firstname, catalogUser.getFirstname(), userDto.firstname);
        Assertions.assertEquals(userDto.lastname, catalogUser.getLastname());
        Assertions.assertEquals(userDto.id, catalogUser.getId());
    }
}