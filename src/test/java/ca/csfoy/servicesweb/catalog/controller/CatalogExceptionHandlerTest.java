package ca.csfoy.servicesweb.catalog.controller;

import ca.csfoy.servicesweb.catalog.controller.exception.DuplicateException;
import ca.csfoy.servicesweb.catalog.controller.exception.ObjectNotFoundException;
import jakarta.validation.ConstraintViolationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.HashSet;
import java.util.List;

@Tag("Api")
class CatalogExceptionHandlerTest {

    private CatalogExceptionHandler exceptionHandler;
    private CatalogProblemDetailFactory problemDetailFactory;

    @BeforeEach
    public void setUp() {
        problemDetailFactory = new CatalogProblemDetailFactory();
        exceptionHandler = new CatalogExceptionHandler(problemDetailFactory);
    }

    @Test
    public void objectNotFoundExceptionWillReturnStatus404() {
        ProblemDetail problemDetail = exceptionHandler.handleObjectNotFoundException(
                new ObjectNotFoundException("Object not found")
        );

        // @TODO ajustez les assertions pour valider les champs pertinents du ProblemDetail reçu
        Assertions.assertTrue(true);
    }

    @Test
    public void DuplicateExceptionWillReturnStatus409() {
        ProblemDetail problemDetail = exceptionHandler.handleDuplicateException(
                new DuplicateException("Duplicate object found")
        );

        // @TODO ajustez les assertions pour valider les champs pertinents du ProblemDetail reçu
        Assertions.assertTrue(true);
    }

    @Test
    public void constraintViolationExceptionWillReturnStatus400() {
        ProblemDetail problemDetail = exceptionHandler.handleConstraintViolationException(
                new ConstraintViolationException(new HashSet<>())
        );

        // @TODO ajustez les assertions pour valider les champs pertinents du ProblemDetail reçu
        Assertions.assertTrue(true);
    }

    @Test
    public void methodArgumentNotValidExceptionWillReturnStatus400() {
        MethodArgumentNotValidException mockException = Mockito.mock(MethodArgumentNotValidException.class);
        BindingResult mockBindingResults = Mockito.mock(BindingResult.class);
        Mockito.when(mockException.getBindingResult()).thenReturn(mockBindingResults);
        Mockito.when(mockBindingResults.getFieldErrors()).thenReturn(
                List.of(new FieldError("object-name", "field-name", "message")));

        ProblemDetail problemDetail = exceptionHandler.handleMethodArgumentNotValid(mockException);

        // @TODO ajustez les assertions pour valider les champs pertinents du ProblemDetail reçu
        Assertions.assertTrue(true);
    }

    @Test
    public void unsupportedMediaTypeWilReturnStatus415() throws Exception {
        ProblemDetail problemDetail =
                exceptionHandler.handleHttpMediaTypeNotSupportedException(new HttpMediaTypeNotSupportedException("Unsupported media type"));

        Assertions.assertEquals(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(), problemDetail.getStatus());
        Assertions.assertEquals(CatalogExceptionHandler.ERROR_TITLE_MSG, problemDetail.getTitle());
    }

    @Test
    public void accessDeniedExceptionWillReturnStatus401() throws Exception {
        ProblemDetail problemDetail =
                exceptionHandler.handleAuthenticationException(new AccessDeniedException("Permission not granted"));

        Assertions.assertEquals(HttpStatus.FORBIDDEN.value(), problemDetail.getStatus());
        Assertions.assertEquals(CatalogExceptionHandler.FORBIDDEN_TITLE_MSG, problemDetail.getTitle());
        Assertions.assertEquals(CatalogExceptionHandler.FORBIDDEN_DETAIL_MSG, problemDetail.getDetail());
    }

    @Test
    public void authenticationExceptionWillReturnStatus401() throws Exception {
        ProblemDetail problemDetail =
                exceptionHandler.handleAuthenticationException(new BadCredentialsException("Bad credentials"));

        Assertions.assertEquals(HttpStatus.UNAUTHORIZED.value(), problemDetail.getStatus());
        Assertions.assertEquals(CatalogExceptionHandler.UNAUTHORIZED_TITLE_MSG, problemDetail.getTitle());
        Assertions.assertEquals(CatalogExceptionHandler.UNAUTHORIZED_DETAIL_MSG, problemDetail.getDetail());
    }

    @Test
    public void unhandledExceptionWillReturnStatus500() throws Exception {
        ProblemDetail problemDetail = exceptionHandler.handleException(new UnhandledException("exception"));

        Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), problemDetail.getStatus());
        Assertions.assertEquals(CatalogExceptionHandler.ERROR_TITLE_MSG, problemDetail.getTitle());
    }

    class UnhandledException extends RuntimeException {
        UnhandledException(String message) {
            super(message);
        }
    }
}