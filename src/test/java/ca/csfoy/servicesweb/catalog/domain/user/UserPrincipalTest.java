package ca.csfoy.servicesweb.catalog.domain.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UserPrincipalTest {

    public static final String USERNAME = "username";
    public static final String ID = "id";

    @Test
    public void newUserPrincipalIsValid() {
        UserPrincipal user = new UserPrincipal(ID, USERNAME);

        Assertions.assertEquals(USERNAME, user.getName());
    }
}