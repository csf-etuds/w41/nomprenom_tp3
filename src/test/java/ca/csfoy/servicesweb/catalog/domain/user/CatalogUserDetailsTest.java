package ca.csfoy.servicesweb.catalog.domain.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

class CatalogUserDetailsTest {

    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String ID = "id";

    @Test
    public void newCatalogUserDetailsWithoutRoleIsValid() {
        CatalogUserDetails details = new CatalogUserDetails(EMAIL, PASSWORD);

        Assertions.assertEquals(RoleName.USER, details.getRole().getRoleName());
        Assertions.assertEquals(EMAIL, details.getUsername());
        Assertions.assertEquals(PASSWORD, details.getPassword());
        Assertions.assertTrue(grantedAuthoritiesToList(details).contains(RoleName.USER.getAuthority()));
        Assertions.assertTrue(details.isAccountNonExpired());
        Assertions.assertTrue(details.isAccountNonLocked());
        Assertions.assertTrue(details.isCredentialsNonExpired());
        Assertions.assertTrue(details.isEnabled());
    }

    @Test
    public void newCatalogUserDetailsWithRoleIsValid() {
        CatalogUserDetails details = new CatalogUserDetails(EMAIL, PASSWORD, new Role(ID, RoleName.ADMIN));

        Assertions.assertEquals(RoleName.ADMIN, details.getRole().getRoleName());
        Assertions.assertEquals(EMAIL, details.getUsername());
        Assertions.assertEquals(PASSWORD, details.getPassword());
        Assertions.assertTrue(grantedAuthoritiesToList(details).contains(RoleName.ADMIN.getAuthority()));
        Assertions.assertTrue(details.isAccountNonExpired());
        Assertions.assertTrue(details.isAccountNonLocked());
        Assertions.assertTrue(details.isCredentialsNonExpired());
        Assertions.assertTrue(details.isEnabled());
    }

    private List<String> grantedAuthoritiesToList(CatalogUserDetails details) {
        return details.getAuthorities().stream().map(GrantedAuthority::getAuthority).toList();
    }

}