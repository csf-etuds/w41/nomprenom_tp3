package ca.csfoy.servicesweb.catalog.domain.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RoleTest {

    public static final String ID = "1";
    public static final String OTHER_ID = "2";
    public static final RoleName ROLE_ADMIN = RoleName.ADMIN;
    public static final RoleName ROLE_USER = RoleName.USER;

    @Test
    public void newRoleWithIdAndNameIsValid() {
        Role role = new Role(ID, ROLE_ADMIN);

        assertEquals(ID, role.getIdentifier());
        assertEquals(ROLE_ADMIN, role.getRoleName());
        assertEquals(ROLE_ADMIN.getAuthority(), role.getAuthority());
    }

    @Test
    public void adminRoleIsAdmin() {
        Role role = new Role(ID, ROLE_ADMIN);

        Assertions.assertTrue(role.isRoleAdmin());
    }

    @Test
    public void adminRoleIsNotUser() {
        Role role = new Role(ID, ROLE_ADMIN);

        Assertions.assertFalse(role.isRoleUser());
    }

    @Test
    public void userRoleIsNotAdmin() {
        Role role = new Role(ID, ROLE_USER);

        Assertions.assertFalse(role.isRoleAdmin());
    }

    @Test
    public void userRoleIsUser() {
        Role role = new Role(ID, ROLE_USER);

        Assertions.assertTrue(role.isRoleUser());
    }

    @Test
    public void canCompareTwoIdenticalRoles() {
        Role role = new Role(ID, ROLE_ADMIN);

        assertEquals(role, role);
    }

    @Test
    public void canCompareTwoDifferentRoles() {
        Role role = new Role(ID, ROLE_ADMIN);
        Role OtherRole = new Role(OTHER_ID, ROLE_USER);

        Assertions.assertNotEquals(role, OtherRole);
    }

    @Test
    public void canCompareARoleAndSomethingElse() {
        Role role = new Role(ID, ROLE_ADMIN);

        Assertions.assertNotEquals(role, new Object());
    }

    @Test
    public void hasHashCode() {
        Role role = new Role(ID, ROLE_ADMIN);

        Assertions.assertNotNull(role.hashCode());
    }

}