package ca.csfoy.servicesweb.catalog.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SearchParamsTest {

    public static final int FIRST_PAGE = 1;
    public static final int NEGATIVE_FIRST_PAGE = -10;
    public static final int JPA_FIRST_PAGE = 0;

    @Test
    public void newSearchParamIsValid() {
        SearchParams searchParams = new ConcreteSearchParams(FIRST_PAGE);

        Assertions.assertEquals(JPA_FIRST_PAGE, searchParams.getPage());
    }

    @Test
    public void pageCannotBeLowerThanZero() {
        SearchParams searchParams = new ConcreteSearchParams(NEGATIVE_FIRST_PAGE);

        Assertions.assertEquals(JPA_FIRST_PAGE, searchParams.getPage());
    }

    class ConcreteSearchParams extends SearchParams {
        public ConcreteSearchParams(int page) {
            super(page);
        }
    }

}