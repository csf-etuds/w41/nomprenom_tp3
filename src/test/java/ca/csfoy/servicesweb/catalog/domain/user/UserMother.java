package ca.csfoy.servicesweb.catalog.domain.user;

import java.util.UUID;

public class UserMother {

    public static final String ANY_USER_ID = UUID.randomUUID().toString();
    public static final String ANY_USER_FIRSTNAME = "Jean";
    public static final String ANY_USER_LASTNAME = "Tremblay";
    public static final String ANY_USER_EMAIL = "jean.tremblay@testcsfoy.ca";
    public static final String ANY_USER_PASSWORD = "this_is_my_super_password1234";
    public static final Role ANY_USER_ROLE = new Role("1", RoleName.USER);

    public static final String ANY_ADMIN_ID = UUID.randomUUID().toString();
    public static final String ANY_ADMIN_FIRSTNAME = "Jannie";
    public static final String ANY_ADMIN_LASTNAME = "Boivin";
    public static final String ANY_ADMIN_EMAIL = "jannie.boivin@testcsfoy.ca";
    public static final String ANY_ADMIN_PASSWORD = "EncryptionisSoFun3426_=";
    public static final Role ANY_ADMIN_ROLE = new Role("2", RoleName.ADMIN);

    public static CatalogUser getUser() {
        return new CatalogUser(ANY_USER_ID, ANY_USER_FIRSTNAME, ANY_USER_LASTNAME,
                new CatalogUserDetails(ANY_USER_EMAIL, ANY_USER_PASSWORD, ANY_USER_ROLE));
    }

    public static CatalogUser getUser(String id) {
        return new CatalogUser(id, ANY_USER_FIRSTNAME, ANY_USER_LASTNAME,
                new CatalogUserDetails(ANY_USER_EMAIL, ANY_USER_PASSWORD, ANY_USER_ROLE));
    }

    public static CatalogUser getAdmin() {
        return new CatalogUser(ANY_ADMIN_ID, ANY_ADMIN_FIRSTNAME, ANY_ADMIN_LASTNAME,
                new CatalogUserDetails(ANY_ADMIN_EMAIL, ANY_ADMIN_PASSWORD, ANY_ADMIN_ROLE));
    }

}
