package ca.csfoy.servicesweb.catalog.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class PaginationSliceTest {

    public static final int FIRST_PAGE_FROM_JPA = 0;
    public static final int FIRST_PAGE = 1;
    public static final int THIRD_PAGE_FROM_JPA = 2;
    public static final int TOTAL_PAGE_ONE_PAGE = 1;
    public static final int NO_ENTRIES = 0;

    public static final int NEGATIVE_CURRENT_PAGE = -10;
    public static final int NEGATIVE_TOTAL_PAGE = -10;
    public static final int NEGATIVE_TOTAL_ENTRIES = -10;

    List<Object> emptyList;

    @BeforeEach
    public void setUp() {
        emptyList = new ArrayList<>();
    }

    @Test
    public void paginationSliceWithoutContentIsValid() {
        PaginationSlice<Object> slice = new PaginationSlice<>(
                emptyList,
                FIRST_PAGE_FROM_JPA,
                TOTAL_PAGE_ONE_PAGE,
                NO_ENTRIES
        );

        Assertions.assertEquals(emptyList, slice.getContent());
        Assertions.assertEquals(FIRST_PAGE, slice.getCurrentPage());
        Assertions.assertEquals(TOTAL_PAGE_ONE_PAGE, slice.getTotalPages());
        Assertions.assertEquals(NO_ENTRIES, slice.getTotalEntries());
    }

    @Test
    public void currentPageCannotBeLessThanOne() {
        PaginationSlice<Object> slice = new PaginationSlice<>(
                emptyList,
                NEGATIVE_CURRENT_PAGE,
                TOTAL_PAGE_ONE_PAGE,
                NO_ENTRIES
        );

        Assertions.assertEquals(FIRST_PAGE, slice.getCurrentPage());
    }

    @Test
    public void totalPageCannotBeLessThanOne() {
        PaginationSlice<Object> slice = new PaginationSlice<>(
                emptyList,
                FIRST_PAGE_FROM_JPA,
                NEGATIVE_TOTAL_PAGE,
                NO_ENTRIES
        );

        Assertions.assertEquals(TOTAL_PAGE_ONE_PAGE, slice.getTotalPages());
    }

    @Test
    public void totalEntriesCannotBeLessThanZero() {
        PaginationSlice<Object> slice = new PaginationSlice<>(
                emptyList,
                FIRST_PAGE_FROM_JPA,
                TOTAL_PAGE_ONE_PAGE,
                NEGATIVE_TOTAL_ENTRIES
        );

        Assertions.assertEquals(TOTAL_PAGE_ONE_PAGE, slice.getTotalPages());
    }

    @Test
    public void currentPageCannotBeGreaterThanMaximumPage() {
        PaginationSlice<Object> slice = new PaginationSlice<>(
                emptyList,
                THIRD_PAGE_FROM_JPA,
                TOTAL_PAGE_ONE_PAGE,
                NO_ENTRIES
        );

        Assertions.assertEquals(TOTAL_PAGE_ONE_PAGE, slice.getCurrentPage());
    }
}