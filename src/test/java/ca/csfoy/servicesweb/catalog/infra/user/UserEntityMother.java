package ca.csfoy.servicesweb.catalog.infra.user;

import ca.csfoy.servicesweb.catalog.domain.user.RoleName;

public class UserEntityMother {
    public static final String ID = "e4008169-d989-4b7d-88fb-11f3bfd8f380";
    public static final String FIRSTNAME = "Joe";
    public static final String LASTNAME = "Bean";
    public static final String EMAIL = "aa@aa.ca";
    public static final String PASSWORD = "correcthorsebatterystaple";

    public static final RoleName ROLE_NAME = RoleName.USER;
    public static final String ROLE_USER_ID = "1";
    public static final String ROLE_ADMIN_ID = "2";

    public static UserEntity userEntity(String id) {
        return new UserEntity(
                id,
                FIRSTNAME,
                LASTNAME,
                EMAIL,
                PASSWORD,
                new RoleEntity(
                        ROLE_USER_ID,
                        ROLE_NAME
                )
        );
    }

}
