package ca.csfoy.servicesweb.catalog.infra.user;

import ca.csfoy.servicesweb.catalog.domain.user.CatalogUser;
import ca.csfoy.servicesweb.catalog.domain.user.Role;
import ca.csfoy.servicesweb.catalog.domain.user.RoleName;
import ca.csfoy.servicesweb.catalog.domain.user.UserMother;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@Tag("Unitaire")
@ExtendWith(MockitoExtension.class)
class UserEntityConverterTest {

    @InjectMocks
    private UserEntityConverter userConverter;

    @Test
    void fromUserThenUserConvertedToEntity() {
        CatalogUser user = UserMother.getUser();

        UserEntity entity = userConverter.fromUser(user);

        Assertions.assertEquals(UserMother.ANY_USER_ID, entity.id);
        Assertions.assertEquals(UserMother.ANY_USER_FIRSTNAME, entity.firstname);
        Assertions.assertEquals(UserMother.ANY_USER_LASTNAME, entity.lastname);
        Assertions.assertEquals(UserMother.ANY_USER_EMAIL, entity.email);
        Assertions.assertEquals(UserMother.ANY_USER_PASSWORD, entity.password);
    }

    @Test
    void toUserThenEntityConvertedToUser() {
        RoleEntity roleEntity = new RoleEntity("2", RoleName.ADMIN);
        UserEntity entity = new UserEntity(UserMother.ANY_ADMIN_ID,
                UserMother.ANY_ADMIN_FIRSTNAME,
                UserMother.ANY_ADMIN_LASTNAME,
                UserMother.ANY_ADMIN_EMAIL,
                UserMother.ANY_ADMIN_PASSWORD,
                roleEntity);

        CatalogUser user = userConverter.toUser(entity);

        Assertions.assertEquals(UserMother.ANY_ADMIN_ID, user.getId());
        Assertions.assertEquals(UserMother.ANY_ADMIN_FIRSTNAME, user.getFirstname());
        Assertions.assertEquals(UserMother.ANY_ADMIN_LASTNAME, user.getLastname());
        Assertions.assertEquals(UserMother.ANY_ADMIN_EMAIL, user.getEmailAddress());
        Assertions.assertEquals(UserMother.ANY_ADMIN_PASSWORD, user.getPassword());
        Assertions.assertEquals(new Role("2", RoleName.ADMIN), user.getRole());
    }
}
