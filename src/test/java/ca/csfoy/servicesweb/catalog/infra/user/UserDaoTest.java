package ca.csfoy.servicesweb.catalog.infra.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@Tag("Data")
@DataJpaTest
class UserDaoTest {

    public static final String EMAIL_ADDRESS = "jean.tremblay@testcsfoy.ca";

    @Autowired
    private UserDao dao;

    @Test
    void findByEmailWhenEmailExistThenUserReturned() {
        UserEntity user = dao.findByEmail(EMAIL_ADDRESS);

        Assertions.assertEquals("5eca874a-62ce-4185-90bd-02ec0b6401c3", user.id);
        Assertions.assertEquals("Jean", user.firstname);
        Assertions.assertEquals("Tremblay", user.lastname);
    }
}
