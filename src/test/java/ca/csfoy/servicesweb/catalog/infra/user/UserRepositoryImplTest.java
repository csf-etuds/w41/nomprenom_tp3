package ca.csfoy.servicesweb.catalog.infra.user;

import ca.csfoy.servicesweb.catalog.controller.exception.DuplicateException;
import ca.csfoy.servicesweb.catalog.controller.exception.ObjectNotFoundException;
import ca.csfoy.servicesweb.catalog.domain.user.CatalogUser;
import ca.csfoy.servicesweb.catalog.domain.user.UserMother;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@Tag("Unitaire")
@ExtendWith(MockitoExtension.class)
class UserRepositoryImplTest {

    @Mock
    private UserDao dao;

    @Mock
    private UserEntityConverter converter;

    @InjectMocks
    private UserRepositoryImpl repo;

    @Test
    void createIfNoDuplicateThenUserIsCreated() {
        CatalogUser user = UserMother.getUser();
        UserEntity returnedEntity = null;
        UserEntity entity = Mockito.mock(UserEntity.class);
        Mockito.when(dao.findByEmail(user.getEmailAddress())).thenReturn(returnedEntity);
        Mockito.when(converter.fromUser(user)).thenReturn(entity);

        repo.create(user);

        Mockito.verify(converter).fromUser(user);
        Mockito.verify(dao).save(entity);
    }

    @Test
    void createIfDuplicateThenDuplicateExceptionThrown() {
        CatalogUser user = UserMother.getUser();
        UserEntity entity = Mockito.mock(UserEntity.class);
        Mockito.when(dao.findByEmail(UserMother.ANY_USER_EMAIL)).thenReturn(entity);

        Assertions.assertThrows(DuplicateException.class, () -> repo.create(user));
    }

    @Test
    void saveThenUserIsSaved() {
        CatalogUser user = UserMother.getUser();
        UserEntity entity = Mockito.mock(UserEntity.class);
        Mockito.when(converter.fromUser(user)).thenReturn(entity);

        repo.save(user.getId(), user);

        Mockito.verify(converter).fromUser(user);
        Mockito.verify(dao).save(entity);
    }

    @Test
    void getIfUserIsPresentThenUserIsReturned() {
        CatalogUser user = UserMother.getUser();
        UserEntity entity = Mockito.mock(UserEntity.class);
        Mockito.when(dao.findById(UserMother.ANY_USER_ID)).thenReturn(Optional.of(entity));
        Mockito.when(converter.toUser(entity)).thenReturn(user);

        CatalogUser userReturned = repo.getBy(UserMother.ANY_USER_ID);

        Mockito.verify(converter).toUser(entity);
        Assertions.assertSame(user, userReturned);
    }

    @Test
    void getIfUserIsNotPresentThenObjectNotFoundExceptionThrown() {
        Mockito.when(dao.findById(UserMother.ANY_USER_ID)).thenReturn(Optional.ofNullable(null));

        Assertions.assertThrows(ObjectNotFoundException.class, () -> repo.getBy(UserMother.ANY_USER_ID));
    }
}
