package ca.csfoy.servicesweb.catalog.infra.user;

import ca.csfoy.servicesweb.catalog.domain.user.RoleName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Unitaire")
class RoleEntityTest {

    @Test
    void roleEntityDefaultConstructorCreatedSuccesfully() {
        // Arrange

        // Act
        new RoleEntity();

        // Assert
    }

    @Test
    void roleEntityUsualConstructorCreatedSuccesfully() {
        // Arrange

        // Act
        new RoleEntity("1", RoleName.USER);

        // Assert
    }

}
