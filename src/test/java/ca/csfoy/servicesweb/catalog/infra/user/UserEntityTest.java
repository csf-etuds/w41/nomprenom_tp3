package ca.csfoy.servicesweb.catalog.infra.user;

import ca.csfoy.servicesweb.catalog.domain.user.RoleName;
import ca.csfoy.servicesweb.catalog.domain.user.UserMother;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Unitaire")
class UserEntityTest {

    @Test
    void userEntityDefaultConstructorCreatedSuccesfully() {
        // Arrange
        // Act
        new UserEntity();
        // Assert
    }

    @Test
    void userEntityUsualConstructorCreatedSuccesfully() {
        // Arrange
        RoleEntity roleEntity = new RoleEntity("1", RoleName.USER);
        // Act
        new UserEntity(UserMother.ANY_USER_ID,
                UserMother.ANY_USER_FIRSTNAME,
                UserMother.ANY_USER_LASTNAME,
                UserMother.ANY_USER_EMAIL,
                UserMother.ANY_USER_PASSWORD,
                roleEntity);
        // Assert
    }

}
