-- Password: Bonjour123 / Role User
INSERT INTO USER_ENTITY (ID, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, ROLE_ID)
VALUES ('7697d20d-6895-4f23-a367-5f1a5302a8a8', 'user', 'basic', 'user@web.ca',
        '$2a$10$UFxICc/wLf.FpwZW1ETpd.51HXCHkwBTosn63ggAx1HNaxzXs3joe', '1');

-- Password: Bonjour123 / Role Admin
INSERT INTO USER_ENTITY (ID, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, ROLE_ID)
VALUES ('52f74a8e-5561-4db6-b475-d16e4a2356d8', 'admin', 'basic', 'admin@web.ca',
        '$2a$10$eW1eTyQKWX32QwLuOpxJi.GMI9B2VMvIX0UbQNTijnOUOi7CEVmXe', '2');

-- ## ROLES
-- Dans le data.sql de l'arboressence main ajoutez les roles manquants

-- ## UTILISATEURS
-- Un utilisateur ayant le rôle de Manager (Récits A à F)
-- Un utilisateur ayant le rôle de Client (Récit G)