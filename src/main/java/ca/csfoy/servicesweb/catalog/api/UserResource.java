package ca.csfoy.servicesweb.catalog.api;

import ca.csfoy.servicesweb.catalog.api.dto.FullUserDto;
import ca.csfoy.servicesweb.catalog.api.dto.UserDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.hibernate.validator.constraints.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Tag(name = "User Resource", description = "API for user manipulation")
@RequestMapping(value = UserResource.RESOURCE_PATH,
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public interface UserResource {

    String RESOURCE_PATH = "/users";
    String PATH_PARAM_ID = "id";
    String PATH_WITH_ID = "/{" + PATH_PARAM_ID + "}";
    String PATH_SIGN_UP = "/signup";

    @Operation(summary = "Allows user creation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User has successfully signed up.",
                    content = { @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "400", description = "Validation errors occurred when creating user.",
                    content = { @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "409", description = "User with identical unique fields already exist.",
                    content = { @Content(mediaType = "application/json") })
    })
    @PostMapping(PATH_SIGN_UP)
    @ResponseStatus(HttpStatus.CREATED)
    void createUser(@Valid @RequestBody FullUserDto user);

    @Operation(summary = "Allows user fetching")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User exists and has been returned.",
                    content = { @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", description = "No user with provided ID exists.",
                    content = { @Content(mediaType = "application/json") })
    })
    @GetMapping(PATH_WITH_ID)
    UserDto getUser(@UUID(message=UserValidationConstants.MSG_UUID_FORMAT_VALIDATION) @PathVariable(PATH_PARAM_ID) String id);

    @Operation(summary = "Allows user modification")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "User has been modified.",
                    content = { @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "400", description = "Validation errors occurred when creating user.",
                    content = { @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", description = "No user with provided ID exists.",
                    content = { @Content(mediaType = "application/json") }),
    })
    @PutMapping(PATH_WITH_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void modifyUser(@UUID(message=UserValidationConstants.MSG_UUID_FORMAT_VALIDATION) @PathVariable(PATH_PARAM_ID) String userId, @Valid @RequestBody UserDto user);
}
