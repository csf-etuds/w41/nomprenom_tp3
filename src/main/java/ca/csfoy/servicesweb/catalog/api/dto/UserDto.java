package ca.csfoy.servicesweb.catalog.api.dto;

import ca.csfoy.servicesweb.catalog.api.UserValidationConstants;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public class UserDto {

    @NotBlank(message = UserValidationConstants.MSG_UUID_NOT_NULL_VALIDATION)
    @Pattern(regexp = UserValidationConstants.FORMAT_UUID, message = UserValidationConstants.MSG_UUID_FORMAT_VALIDATION)
    public final String id;

    @NotBlank(message = UserValidationConstants.MSG_STR_NOT_NULL_VALIDATION)
    @Size(min = 1, max = 30, message = UserValidationConstants.MSG_STR_MIN_MAX_LENGTH_VALIDATION)
    public final String firstname;

    @NotBlank(message = UserValidationConstants.MSG_STR_NOT_NULL_VALIDATION)
    @Size(min = 1, max = 30, message = UserValidationConstants.MSG_STR_MIN_MAX_LENGTH_VALIDATION)
    public final String lastname;

    public UserDto(String id, String firstname, String lastname) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }
}