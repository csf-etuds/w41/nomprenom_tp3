package ca.csfoy.servicesweb.catalog.api.dto;

import ca.csfoy.servicesweb.catalog.api.UserValidationConstants;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public class FullUserDto {

    @Pattern(regexp = UserValidationConstants.FORMAT_UUID, message = UserValidationConstants.MSG_UUID_FORMAT_VALIDATION)
    public final String id;

    @NotBlank(message = UserValidationConstants.MSG_STR_NOT_NULL_VALIDATION)
    @Size(min = 1, max = 30, message = UserValidationConstants.MSG_STR_MIN_MAX_LENGTH_VALIDATION)
    public final String firstname;

    @NotBlank(message = UserValidationConstants.MSG_STR_NOT_NULL_VALIDATION)
    @Size(min = 1, max = 30, message = UserValidationConstants.MSG_STR_MIN_MAX_LENGTH_VALIDATION)
    public final String lastname;

    @NotBlank(message = UserValidationConstants.MSG_EMAIL_NOT_NULL_VALIDATION)
    @Email(message = UserValidationConstants.MSG_FORMAT_EMAIL_VALIDATION)
    public final String emailAddress;

    @NotBlank(message = UserValidationConstants.MSG_PWD_NOT_NULL_VALIDATION)
    /*
     * @TODO ajuster le validateur de taille
     *   Ajustez les tests liés à la taille du mot de passe
     */
    @Size(max = 64, message = UserValidationConstants.MSG_PWD_MAX_LENGTH_VALIDATION)
    /*
     * @TODO Ajouter un validateur pour la force du mot de passe
     * @TODO Ajouter les tests unitaires nécessaires dans le test de DTO
     * @TODO Créer les tests unitaires requis pour tester votre validateur de mot de passe
     *   * 1 cas pour un mot de passe valide
     *   * 1 cas pour un mot de passe invalide
     */
    public final String password;

    public FullUserDto(String id,
                       String firstname,
                       String lastname,
                       String emailAddress,
                       String password
    ) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.emailAddress = emailAddress;
        this.password = password;
    }
}
