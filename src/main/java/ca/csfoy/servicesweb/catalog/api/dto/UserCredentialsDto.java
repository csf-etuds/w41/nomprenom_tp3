package ca.csfoy.servicesweb.catalog.api.dto;

public class UserCredentialsDto {

    public final String emailAddress;
    public final String password;

    public UserCredentialsDto(String emailAddress, String password) {
        this.emailAddress = emailAddress;
        this.password = password;
    }
}
