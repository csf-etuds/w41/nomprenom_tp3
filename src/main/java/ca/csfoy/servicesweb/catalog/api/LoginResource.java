package ca.csfoy.servicesweb.catalog.api;

import ca.csfoy.servicesweb.catalog.api.dto.TokenDto;
import ca.csfoy.servicesweb.catalog.api.dto.UserCredentialsDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Tag(name = "Login Resource", description = "API for user login")
@RequestMapping(value = LoginResource.RESOURCE_PATH,
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public interface LoginResource {

    String RESOURCE_PATH = UserResource.RESOURCE_PATH + "/login";

    @Operation(summary = "Allows users to login")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User credentials have been validated and JWT token has been returned.",
                    content = { @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "401", description = "Impossible to authenticate with provided credentials.",
                    content = { @Content(mediaType = "application/json") })
    })
    @PostMapping
    TokenDto loginUser(@RequestBody UserCredentialsDto user);
}
