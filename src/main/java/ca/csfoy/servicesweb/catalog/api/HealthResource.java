package ca.csfoy.servicesweb.catalog.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Tag(name = "Health Resource", description = "API for the Health Resource")
@RequestMapping(value = HealthResource.RESOURCE_PATH,
        produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_PROBLEM_JSON_VALUE },
        consumes = MediaType.APPLICATION_JSON_VALUE)
public interface HealthResource {

    String RESOURCE_PATH = "/health";

    @Operation(summary = "Check if application is working properly")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The system is up and running.",
                    content = { @Content(mediaType = "application/json") }) })
    @GetMapping
    void getHealth();

}
