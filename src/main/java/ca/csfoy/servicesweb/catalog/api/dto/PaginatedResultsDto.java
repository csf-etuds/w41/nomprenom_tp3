package ca.csfoy.servicesweb.catalog.api.dto;

import java.util.List;

/**
 * Class to return paginated results.
 *
 * The first page should have index 1 to external users (JPA starts at page 0)
 *
 * @param <T>
 *         Type of the values returned
 */
public class PaginatedResultsDto<T> {

    private final List<T> content;
    private final int currentPage;
    private final int totalPages;
    private final long totalEntries;

    public PaginatedResultsDto(List<T> content, int currentPage, int totalPages, long totalEntries) {
        this.content = content;
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.totalEntries = totalEntries;
    }

    public List<T> getContent() {
        return content;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public long getTotalEntries() {
        return totalEntries;
    }
}
