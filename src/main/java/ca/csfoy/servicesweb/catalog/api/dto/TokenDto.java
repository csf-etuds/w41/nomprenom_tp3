package ca.csfoy.servicesweb.catalog.api.dto;

public class TokenDto {

    public String token;

    public TokenDto(String token) {
        this.token = token;
    }

    public TokenDto() {
    }
}
