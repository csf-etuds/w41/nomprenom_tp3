package ca.csfoy.servicesweb.catalog.security;

import ca.csfoy.servicesweb.catalog.domain.user.Role;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;

@Component
public class JwtTokenManager {

    private final Environment env;

    @Autowired
    public JwtTokenManager(Environment env) {
        this.env = env;
    }

    public String createToken(String email, Role role) {
        Claims claims = Jwts.claims().setSubject(email);
        claims.put("auth", role);
        Date now = new Date();
        return Jwts.builder()
                   .setClaims(claims)
                   .setIssuedAt(now)
                   .setExpiration(
                           new Date(now.getTime() + Long.parseLong(env.getProperty("jwt.validityTimeInMilliseconds"))))
                   .signWith(Keys.hmacShaKeyFor(encodeSecret()), SignatureAlgorithm.HS384)
                   .compact();
    }

    public String getUserIdFromToken(String token) {
        return (String) this.getClaims(token).get("id");
    }

    public String getSubjectFromToken(String token) {
        return this.getClaims(token).getSubject();
    }

    public boolean validateToken(String token) {
        this.getClaims(token);
        return true;
    }

    private Claims getClaims(String token) {
        return Jwts.parserBuilder()
                   .setSigningKey(Keys.hmacShaKeyFor(encodeSecret()))
                   .build()
                   .parseClaimsJws(token)
                   .getBody();
    }

    private byte[] encodeSecret() {
        return Base64.getEncoder().encode(env.getProperty("jwt.secretKey").getBytes());
    }
}
