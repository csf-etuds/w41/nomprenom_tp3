package ca.csfoy.servicesweb.catalog.security;

import ca.csfoy.servicesweb.catalog.controller.CatalogProblemDetailFactory;
import ca.csfoy.servicesweb.catalog.controller.LoggerUtils;
import ca.csfoy.servicesweb.catalog.domain.user.UserPrincipal;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ProblemDetail;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

@Component
public class JwtTokenEntryPointFilter extends OncePerRequestFilter {

    private final CatalogProblemDetailFactory problemDetailFactory;
    private final JwtTokenManager tokenManager;
    private final UserDetailsService userDetailsService;
    private final ObjectMapper objectMapper;
    private final Logger logger = LoggerFactory.getLogger(JwtTokenEntryPointFilter.class);

    public JwtTokenEntryPointFilter(CatalogProblemDetailFactory problemDetailFactory,
                                    JwtTokenManager tokenManager,
                                    UserDetailsService userDetailsService,
                                    ObjectMapper objectMapper
    ) {
        this.problemDetailFactory = problemDetailFactory;
        this.tokenManager = tokenManager;
        this.userDetailsService = userDetailsService;
        this.objectMapper = objectMapper;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String token = request.getHeader("Authorization");
        try {
            this.authenticate(token);
        } catch (RuntimeException authException) {
            this.setExceptionResponse(response, authException);
            return;
        }

        filterChain.doFilter(request, response);
    }

    private void authenticate(String token) {
        if (Objects.isNull(token)) {
            return;
        }

        String tokenWithoutBearer = token;
        if (token.startsWith("Bearer ")) {
            tokenWithoutBearer = token.substring(7);
        }

        tokenManager.validateToken(tokenWithoutBearer);
        String email = tokenManager.getSubjectFromToken(tokenWithoutBearer);
        String id = tokenManager.getUserIdFromToken(tokenWithoutBearer);
        UserDetails user = userDetailsService.loadUserByUsername(email);

        SecurityContextHolder.getContext()
                             .setAuthentication(new UsernamePasswordAuthenticationToken(
                                     new UserPrincipal(id, user.getUsername()),
                                     user.getPassword(),
                                     user.getAuthorities()));

    }

    private void setExceptionResponse(HttpServletResponse response, Exception authException) {
        SecurityContextHolder.clearContext();
        String errorId = UUID.randomUUID().toString();
        ProblemDetail problemDetail =
                problemDetailFactory.createProblemDetail(authException, HttpStatus.FORBIDDEN, errorId, true);
        LoggerUtils.logError(logger, authException, errorId);
        try {
            setResponse(response, problemDetail);
        } catch (IOException jpe) {
            throw new RuntimeException("An error occured in creating authentication exception response", jpe);
        }
    }

    private void setResponse(HttpServletResponse response, ProblemDetail problemDetail)
            throws IOException {
        response.setStatus(problemDetail.getStatus());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding("UTF-8");
        response.getOutputStream().println(objectMapper.writeValueAsString(problemDetail));
    }
}
