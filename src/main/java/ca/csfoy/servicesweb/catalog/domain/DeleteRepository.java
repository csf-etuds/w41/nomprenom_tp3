package ca.csfoy.servicesweb.catalog.domain;

/**
 * Dépôt de base avec une fonctionnalité ajoutée de modification.
 *
 * @param <I>
 * @param <E>
 * @author cboileau
 */
public interface DeleteRepository<I, E> extends BasicRepository<I, E> {

    void delete(I id);
}
