package ca.csfoy.servicesweb.catalog.domain;

import java.util.List;

/**
 * Class to simplify Spring Page<T>. JPA starts pagination on page 0, we want to return a pagination that starts
 * at 1.
 *
 * @param <T>
 *         Type of objects returned
 */
public class PaginationSlice<T> {

    private final List<T> content;
    private final int currentPage;
    private final int totalPages;
    private final long totalEntries;

    public PaginationSlice(List<T> content, int currentPage, int totalPages, long totalEntries) {
        this.content = content;
        this.totalPages = Math.max(1, totalPages);
        this.currentPage = Math.min(Math.max(1, currentPage + 1), this.totalPages);
        this.totalEntries = Math.max(0, totalEntries);
    }

    public List<T> getContent() {
        return content;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public long getTotalEntries() {
        return totalEntries;
    }
}