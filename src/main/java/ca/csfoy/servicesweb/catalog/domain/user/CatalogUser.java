package ca.csfoy.servicesweb.catalog.domain.user;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class CatalogUser {

    private static final long serialVersionUID = 1L;

    private final String id;
    private final CatalogUserDetails userDetails;
    private String firstname;
    private String lastname;

    public CatalogUser(String id,
                       String firstname,
                       String lastname,
                       CatalogUserDetails userDetails
    ) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.userDetails = userDetails;
    }

    public String getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void replaceFirstname(String value) {
        if (value != null && !value.isBlank()) {
            firstname = value;
        }
    }

    public String getLastname() {
        return lastname;
    }

    public void replaceLastname(String value) {
        if (value != null && !value.isBlank()) {
            lastname = value;
        }
    }

    public String getEmailAddress() {
        return this.userDetails.getUsername();
    }

    public String getPassword() {
        return this.userDetails.getPassword();
    }

    public Role getRole() {
        return this.userDetails.getRole();
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.userDetails.getAuthorities();
    }
}
