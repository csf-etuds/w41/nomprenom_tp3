package ca.csfoy.servicesweb.catalog.domain.user;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

public class CatalogUserDetails implements UserDetails {

    private static final long serialVersionUID = 1L;
    private final String emailAddress;
    private final String password;
    private final Role role;

    public CatalogUserDetails(String emailAddress, String password) {
        this.emailAddress = emailAddress;
        this.password = password;
        this.role = new Role("1", RoleName.USER);
    }

    public CatalogUserDetails(String emailAddress, String password, Role role) {
        this.emailAddress = emailAddress;
        this.password = password;
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return emailAddress;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(this.role.getAuthority()));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
