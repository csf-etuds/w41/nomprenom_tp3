package ca.csfoy.servicesweb.catalog.domain;

/**
 * Class to search objects with pagination
 *
 * The first page should have index 0 for JPA pagination (external pagination should start at 1)
 */
public abstract class SearchParams {

    private int page;

    public SearchParams(int page) {
        this.page = Math.max(0, page - 1);
    }

    public int getPage() {
        return page;
    }
}
