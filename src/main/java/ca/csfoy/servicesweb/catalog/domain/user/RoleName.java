package ca.csfoy.servicesweb.catalog.domain.user;

public enum RoleName {
    ADMIN,
    USER;

    private final String authority;

    RoleName() {
        this.authority = "ROLE_" + this;
    }

    public String getAuthority() {
        return this.authority;
    }
}
