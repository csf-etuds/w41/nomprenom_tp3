package ca.csfoy.servicesweb.catalog.domain.user;

import java.util.Objects;

public class Role {

    private final String identifier;
    private final RoleName roleName;

    public Role(String identifier, RoleName roleName) {

        this.identifier = identifier;
        this.roleName = roleName;
    }

    public String getIdentifier() {
        return identifier;
    }

    public RoleName getRoleName() {
        return this.roleName;
    }

    public boolean isRoleAdmin() {
        return roleName == RoleName.ADMIN;
    }

    public boolean isRoleUser() {
        return roleName == RoleName.USER;
    }

    public String getAuthority() {
        return roleName.getAuthority();
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier, roleName);
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object instanceof Role that) {

            return Objects.equals(this.identifier, that.identifier) && Objects.equals(this.roleName, that.roleName);
        }

        return false;
    }
}
