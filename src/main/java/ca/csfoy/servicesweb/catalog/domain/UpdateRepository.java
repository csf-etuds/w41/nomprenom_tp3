package ca.csfoy.servicesweb.catalog.domain;

/**
 * Dépôt de base avec une fonctionnalité ajoutée de modification.
 *
 * @param <I>
 *         Le type de l'identifiant des objets stockés dans le dépôt
 * @param <E>
 *         Le type de l'objet stocké dans le dépôt
 * @author cboileau
 */
public interface UpdateRepository<I, E> extends BasicRepository<I, E> {

    void save(I id, E element);
}
