package ca.csfoy.servicesweb.catalog.domain.user;

import ca.csfoy.servicesweb.catalog.domain.UpdateRepository;

public interface UserRepository extends UpdateRepository<String, CatalogUser> {

    CatalogUser getByEmail(String email);
}
