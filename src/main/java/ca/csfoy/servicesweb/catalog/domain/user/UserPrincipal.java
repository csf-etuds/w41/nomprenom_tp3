package ca.csfoy.servicesweb.catalog.domain.user;

import java.security.Principal;

public class UserPrincipal implements Principal {

    public final String id;
    public final String username;

    public UserPrincipal(String id, String username) {
        this.id = id;
        this.username = username;
    }

    @Override
    public String getName() {
        return username;
    }
}