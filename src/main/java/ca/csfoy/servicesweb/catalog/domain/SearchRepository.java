package ca.csfoy.servicesweb.catalog.domain;

public interface SearchRepository<I, E, P extends SearchParams> extends BasicRepository<I, E> {

    int ENTRIES_PER_PAGE = 10;

    PaginationSlice<E> search(P params);
}
