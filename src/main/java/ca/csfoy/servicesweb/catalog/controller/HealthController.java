package ca.csfoy.servicesweb.catalog.controller;

import ca.csfoy.servicesweb.catalog.api.HealthResource;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController implements HealthResource {

    @Override
    public void getHealth() {

    }
}
