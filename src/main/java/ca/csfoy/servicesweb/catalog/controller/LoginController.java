package ca.csfoy.servicesweb.catalog.controller;

import ca.csfoy.servicesweb.catalog.api.LoginResource;
import ca.csfoy.servicesweb.catalog.api.dto.TokenDto;
import ca.csfoy.servicesweb.catalog.api.dto.UserCredentialsDto;
import ca.csfoy.servicesweb.catalog.domain.user.CatalogUser;
import ca.csfoy.servicesweb.catalog.domain.user.UserRepository;
import ca.csfoy.servicesweb.catalog.security.JwtTokenManager;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController implements LoginResource {

    private final UserRepository repo;
    private final AuthenticationManager authManager;
    private final JwtTokenManager tokenManager;

    public LoginController(UserRepository repo, AuthenticationManager authManager, JwtTokenManager tokenManager) {
        this.repo = repo;
        this.authManager = authManager;
        this.tokenManager = tokenManager;
    }

    @Override
    public TokenDto loginUser(UserCredentialsDto user) {
        authManager.authenticate(new UsernamePasswordAuthenticationToken(user.emailAddress, user.password));

        CatalogUser catalogUser = repo.getByEmail(user.emailAddress);

        String token = tokenManager.createToken(catalogUser.getEmailAddress(), catalogUser.getRole());

        return new TokenDto(token);
    }
}
