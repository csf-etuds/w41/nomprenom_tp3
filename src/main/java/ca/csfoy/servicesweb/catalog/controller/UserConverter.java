package ca.csfoy.servicesweb.catalog.controller;

import ca.csfoy.servicesweb.catalog.api.dto.FullUserDto;
import ca.csfoy.servicesweb.catalog.api.dto.UserDto;
import ca.csfoy.servicesweb.catalog.domain.user.CatalogUser;
import ca.csfoy.servicesweb.catalog.domain.user.CatalogUserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.UUID;

@Component
public class UserConverter {

    private final PasswordEncoder passwordEncoder;

    public UserConverter(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public UserDto fromUser(CatalogUser user) {
        return new UserDto(user.getId(),
                user.getFirstname(),
                user.getLastname());
    }

    public CatalogUser toUser(FullUserDto user) {
        if (Objects.isNull(user.id)) {
            return new CatalogUser(UUID.randomUUID().toString(),
                    user.firstname,
                    user.lastname,
                    new CatalogUserDetails(user.emailAddress, passwordEncoder.encode(user.password)));
        }

        return new CatalogUser(user.id,
                user.firstname,
                user.lastname,
                new CatalogUserDetails(user.emailAddress, passwordEncoder.encode(user.password)));
    }
}
