package ca.csfoy.servicesweb.catalog.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
public class CatalogProblemDetailFactory {

    public static final String DEV_API_DOCUMENTATION = "http://localhost:8080/swagger-ui/index.html";

    public ProblemDetail createProblemDetail(Throwable e, HttpStatus statusCode, String errorId, boolean expected) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(statusCode, e.getMessage());
        problemDetail.setType(URI.create(DEV_API_DOCUMENTATION));
        problemDetail.setProperty("expected", expected);
        problemDetail.setProperty("id", errorId);
        return problemDetail;
    }
}
