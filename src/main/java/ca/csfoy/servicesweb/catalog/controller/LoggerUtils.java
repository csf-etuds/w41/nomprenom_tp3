package ca.csfoy.servicesweb.catalog.controller;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;

import java.time.LocalDateTime;

public class LoggerUtils {

    public static final String LOGGING_FORMAT = "[%s](id:%s)-> Message:%s, Trace:%s";

    public static void logError(Logger logger, Throwable ex, String errorId) {
        String timestamp = LocalDateTime.now().toString();
        String mesg = ex.getMessage();
        String stack = ExceptionUtils.getStackTrace(ex);
        logger.error(String.format(LOGGING_FORMAT,
                timestamp,
                errorId,
                mesg,
                stack));
    }

}
