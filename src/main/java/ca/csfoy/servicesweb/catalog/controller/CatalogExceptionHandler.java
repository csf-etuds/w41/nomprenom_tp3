package ca.csfoy.servicesweb.catalog.controller;

import ca.csfoy.servicesweb.catalog.controller.exception.DuplicateException;
import ca.csfoy.servicesweb.catalog.controller.exception.ObjectNotFoundException;
import jakarta.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestControllerAdvice
public class CatalogExceptionHandler {

    public static final String DUPLICATE_TITLE_MSG = "This resource already exist on the server.";
    public static final String OBJECT_NOT_FOUND_TITLE_MSG = "An object requested was not found on the server.";
    public static final String CONSTRAINT_VIOLATION_TITLE_MSG =
            "An input does not comply with a validation constraint.";
    public static final String CONSTRAINT_VIOLATION_DETAIL_MSG = "Constraints not respected, see details in field errors";
    public static final String INPUT_ARG_TITLE_MSG =
            "The provided input does not comply to one or more validations contraints.";
    public static final String INPUT_ARG_DETAIL_MSG = "Inputs invalid, see details in field errors";
    public static final String UNAUTHORIZED_TITLE_MSG = "Access denied";
    public static final String UNAUTHORIZED_DETAIL_MSG = "Impossible to process login request, verify provided credentials and try again";
    public static final String FORBIDDEN_TITLE_MSG = "Access denied";
    public static final String FORBIDDEN_DETAIL_MSG = "You do not have the permission to complete this action";
    public static final String ERROR_TITLE_MSG = "An error has occured.";

    private final CatalogProblemDetailFactory problemDetailFactory;

    private final Logger logger = LoggerFactory.getLogger(CatalogExceptionHandler.class);

    @Autowired
    public CatalogExceptionHandler(CatalogProblemDetailFactory problemDetailFactory) {
        this.problemDetailFactory = problemDetailFactory;
    }

    /*
     * @TODO Gestion des erreurs pour les exceptions possibles
     *   * Créer un ProblemDetail adéquat (à l'aide de CatalogProblemDetailFactory)
     *     * Code HTTP
     *     * Id pour l'erreur
     *     * Statut de l'erreur (attendu / inattendue)
     *   * Assignez un titre au problem detail
     *     * Utilisez les constantes au début de ce fichier (TITLE)
     *   * Lorsque le détail est trop difficile à lire (trop long / caractère bizarres pour un utilisateur) assignez un détail plus simple
     *     * Utilisez les constantes au début du fichier (DETAIL)
     *   * Pour les exceptions MethodArgumentNotValidException et ConstraintViolationException utilisez les méthodes utilitaires fournies dans le bas de ce ficher
     *     * MethodArgumentNotValidException -> extractMethodArgumentNotValid
     *     * ConstraintViolationException -> extractConstraintViolations
     *   * Journalisez les erreurs
     *     * N'oubliez pas la configuration dans le fichier Application.properties
     *   * Mettez à jour les tests correspondants dans la classe de test ca.csfoy.servicesweb.catalog.controller.CatalogExceptionHandlerTest
     */

    public ProblemDetail handleDuplicateException(DuplicateException ex) {
        // @TODO implémentation
        // @TODO tests unitaires
        return null;
    }

    public ProblemDetail handleObjectNotFoundException(ObjectNotFoundException ex) {
        // @TODO implémentation
        // @TODO tests unitaires
        return null;
    }

    public ProblemDetail handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        // @TODO implémentation
        // @TODO tests unitaires
        return null;
    }

    public ProblemDetail handleConstraintViolationException(ConstraintViolationException ex) {
        // @TODO implémentation
        // @TODO tests unitaires
        return null;
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ProblemDetail handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException ex) {
        String errorId = UUID.randomUUID().toString();
        ProblemDetail pd =
                problemDetailFactory.createProblemDetail(ex, HttpStatus.UNSUPPORTED_MEDIA_TYPE, errorId, true);
        pd.setTitle(CatalogExceptionHandler.ERROR_TITLE_MSG);
        LoggerUtils.logError(logger, ex, errorId);
        return pd;
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ProblemDetail handleAuthenticationException(AccessDeniedException ex) {
        String errorId = UUID.randomUUID().toString();
        ProblemDetail pd = problemDetailFactory.createProblemDetail(ex, HttpStatus.FORBIDDEN, errorId, false);
        pd.setTitle(CatalogExceptionHandler.FORBIDDEN_TITLE_MSG);
        pd.setDetail(FORBIDDEN_DETAIL_MSG);
        LoggerUtils.logError(logger, ex, errorId);
        return pd;
    }

    @ExceptionHandler(AuthenticationException.class)
    public ProblemDetail handleAuthenticationException(AuthenticationException ex) {
        String errorId = UUID.randomUUID().toString();
        ProblemDetail pd = problemDetailFactory.createProblemDetail(ex, HttpStatus.UNAUTHORIZED, errorId, false);
        pd.setTitle(CatalogExceptionHandler.UNAUTHORIZED_TITLE_MSG);
        pd.setDetail(UNAUTHORIZED_DETAIL_MSG);
        LoggerUtils.logError(logger, ex, errorId);
        return pd;
    }

    @ExceptionHandler(Throwable.class)
    public ProblemDetail handleException(Exception ex) {
        String errorId = UUID.randomUUID().toString();
        ProblemDetail pd =
                problemDetailFactory.createProblemDetail(ex, HttpStatus.INTERNAL_SERVER_ERROR, errorId, false);
        pd.setTitle(CatalogExceptionHandler.ERROR_TITLE_MSG);
        LoggerUtils.logError(logger, ex, errorId);
        return pd;
    }

    private Map<String, List<String>> extractMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        Map<String, List<String>> errors = new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(e -> {
            String key = e.getField();
            if (!errors.containsKey(key)) {
                errors.put(key, new ArrayList<>());
            }
            errors.get(key).add(e.getDefaultMessage());
        });

        return errors;
    }

    private Map<String, List<String>> extractConstraintViolations(ConstraintViolationException ex) {
        Map<String, List<String>> errors = new HashMap<>();
        ex.getConstraintViolations().forEach(c -> {
            String key = c.getPropertyPath().toString();
            if (!errors.containsKey(key)) {
                errors.put(key, new ArrayList<>());
            }
            errors.get(key).add(c.getMessage());
        });

        return errors;
    }

}
