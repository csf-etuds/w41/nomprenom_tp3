package ca.csfoy.servicesweb.catalog.controller;

import ca.csfoy.servicesweb.catalog.api.UserResource;
import ca.csfoy.servicesweb.catalog.api.dto.FullUserDto;
import ca.csfoy.servicesweb.catalog.api.dto.UserDto;
import ca.csfoy.servicesweb.catalog.domain.user.CatalogUser;
import ca.csfoy.servicesweb.catalog.domain.user.UserRepository;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController implements UserResource {

    private final UserRepository repo;
    private final UserConverter converter;

    public UserController(UserRepository repo, UserConverter converter) {
        this.repo = repo;
        this.converter = converter;
    }

    @Override
    public void createUser(FullUserDto user) {

        repo.create(converter.toUser(user));
    }

    @Override
    public UserDto getUser(String id) {
        return converter.fromUser(repo.getBy(id));
    }

    @Override
    public void modifyUser(String id, UserDto userDto) {
        CatalogUser user = repo.getBy(id);

        user.replaceFirstname(userDto.firstname);
        user.replaceLastname(userDto.lastname);

        repo.save(id, user);
    }

}
