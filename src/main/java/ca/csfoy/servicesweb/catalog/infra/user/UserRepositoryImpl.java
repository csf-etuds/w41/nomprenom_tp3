package ca.csfoy.servicesweb.catalog.infra.user;

import ca.csfoy.servicesweb.catalog.controller.exception.DuplicateException;
import ca.csfoy.servicesweb.catalog.controller.exception.ObjectNotFoundException;
import ca.csfoy.servicesweb.catalog.domain.user.CatalogUser;
import ca.csfoy.servicesweb.catalog.domain.user.UserRepository;
import org.springframework.stereotype.Repository;

import java.util.Objects;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final UserDao dao;
    private final UserEntityConverter converter;

    public UserRepositoryImpl(UserDao dao, UserEntityConverter converter) {
        this.dao = dao;
        this.converter = converter;
    }

    @Override
    public CatalogUser create(CatalogUser user) {
        if (Objects.nonNull(dao.findByEmail(user.getEmailAddress()))) {
            throw new DuplicateException("User (email: " + user.getEmailAddress() + ") already exists.");
        }

        UserEntity entity = dao.save(converter.fromUser(user));
        return converter.toUser(entity);
    }

    @Override
    public void save(String id, CatalogUser user) {
        dao.save(converter.fromUser(user));
    }

    @Override
    public CatalogUser getBy(String id) {
        Optional<UserEntity> user = dao.findById(id);
        if (!user.isPresent())
            throw new ObjectNotFoundException("User (id:" + id + ") does not exist.");

        return converter.toUser(user.get());
    }

    @Override
    public CatalogUser getByEmail(String email) {
        UserEntity user = dao.findByEmail(email);
        if (Objects.isNull(user))
            throw new ObjectNotFoundException("User (email:" + email + ") does not exist.");

        return converter.toUser(user);
    }

}
