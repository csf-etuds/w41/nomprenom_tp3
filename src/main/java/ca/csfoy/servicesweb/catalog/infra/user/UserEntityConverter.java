package ca.csfoy.servicesweb.catalog.infra.user;

import ca.csfoy.servicesweb.catalog.domain.user.CatalogUser;
import ca.csfoy.servicesweb.catalog.domain.user.CatalogUserDetails;
import ca.csfoy.servicesweb.catalog.domain.user.Role;
import org.springframework.stereotype.Component;

@Component
public class UserEntityConverter {

    public UserEntity fromUser(CatalogUser user) {
        return new UserEntity(user.getId(),
                user.getFirstname(),
                user.getLastname(),
                user.getEmailAddress(),
                user.getPassword(),
                fromRole(user.getRole()));
    }

    public CatalogUser toUser(UserEntity user) {
        return new CatalogUser(user.id,
                user.firstname,
                user.lastname,
                new CatalogUserDetails(user.email,
                        user.password,
                        toRole(user.role)));
    }

    public RoleEntity fromRole(Role role) {
        return new RoleEntity(role.getIdentifier(), role.getRoleName());
    }

    public Role toRole(RoleEntity role) {
        return new Role(role.id, role.roleName);
    }
}
