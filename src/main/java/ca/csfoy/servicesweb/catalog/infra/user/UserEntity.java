package ca.csfoy.servicesweb.catalog.infra.user;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class UserEntity {

    @Id
    public String id;
    @Column(length = 50, nullable = false)
    public String firstname;
    @Column(length = 50, nullable = false)
    public String lastname;
    @Column(length = 70, nullable = false)
    public String email;
    @Column(length = 70, nullable = false)
    public String password;
    @ManyToOne
    @JoinColumn(name = "role_id")
    public RoleEntity role;

    UserEntity() {
    }

    public UserEntity(String id,
                      String firstname,
                      String lastname,
                      String emailAddress,
                      String password,
                      RoleEntity role
    ) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = emailAddress;
        this.password = password;
        this.role = role;
    }
}
