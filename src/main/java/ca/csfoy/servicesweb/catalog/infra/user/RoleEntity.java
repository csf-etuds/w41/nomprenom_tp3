package ca.csfoy.servicesweb.catalog.infra.user;

import ca.csfoy.servicesweb.catalog.domain.user.RoleName;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

import java.util.Set;

@Entity
public class RoleEntity {

    @Id
    public String id;

    @Enumerated(EnumType.STRING)
    public RoleName roleName;

    @OneToMany(targetEntity = UserEntity.class, mappedBy = "role", cascade = CascadeType.ALL)
    Set<UserEntity> users;

    public RoleEntity() {

    }

    public RoleEntity(String identifier, RoleName roleName) {
        this.id = identifier;
        this.roleName = roleName;
    }
}
