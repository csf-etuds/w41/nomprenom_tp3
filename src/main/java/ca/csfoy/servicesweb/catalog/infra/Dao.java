package ca.csfoy.servicesweb.catalog.infra;

import java.util.List;

public interface Dao<I, E> {

    boolean doesExist(I id);

    E selectById(I id);

    List<E> selectAll();

    E insert(E element);

    void update(I id, E element);

    void delete(I id);
}
