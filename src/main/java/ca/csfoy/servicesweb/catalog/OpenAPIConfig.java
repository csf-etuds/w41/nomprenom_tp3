package ca.csfoy.servicesweb.catalog;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenAPIConfig {

    @Bean
    public OpenAPI myOpenAPI() {
        /* @TODO Ajoutez les informations de tous les membres de l'équipe
         *   * Utilisez le format Membre A | Membre B (utilisez le même ordre que dans le README.md
         */
        Contact contact = new Contact();

        License mitLicense = new License().name("MIT License").url("https://choosealicense.com/licenses/mit/");

        /* @TODO Ajustez les informations
         *   Le titre doit être le titre du projet (tel qu'écrit dans le README.md)
         *   la version doit être la version 1.0.0
         *   Ajoutez le contact, une description et la licence
         */
        Info info = new Info()
                .title("Base title")
                .version("0.0.1");

        return new OpenAPI().info(info);
    }
}
