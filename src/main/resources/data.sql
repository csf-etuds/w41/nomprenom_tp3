insert into ROLE_ENTITY(ID, ROLE_NAME)
values ('1', 'ADMIN');

insert into ROLE_ENTITY(ID, ROLE_NAME)
values ('2', 'USER');

-- ## ROLES (Récits A à G)
-- Manager (Récits A à E)
-- Client (Récit F)

/** bcrypt: Bonjour12345 / role Admin */
insert into USER_ENTITY(ID, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, ROLE_ID)
values ('5eca874a-62ce-4185-90bd-02ec0b6401c3', 'Jean', 'Tremblay', 'jean.tremblay@testcsfoy.ca',
        '$2a$10$813aNBOvhkxkAkmfP0b.5u40gj7ll1uJExpUBAzhCoYTJqDccA23S', 1);

/** bcrypt: Bonjour12345 / role User */
insert into USER_ENTITY(ID, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, ROLE_ID)
values ('1752aedc-ca8c-4f79-9441-6d4ff16e2c4f', 'Julie', 'Tremblay', 'julie.tremblay@testcsfoy.ca',
        '$2a$10$813aNBOvhkxkAkmfP0b.5u40gj7ll1uJExpUBAzhCoYTJqDccA23S', 2);

-- ## UTILISATEURS
-- Un utilisateur ayant le rôle de Manager (Récits A à F)
-- Un utilisateur ayant le rôle de Client (Récit G)